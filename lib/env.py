from dotenv import dotenv_values

ENVFILE = ".env"

def get_env(key: str) -> str:
    """A helper function to check if any required value in ENVFILE is missing.
    The default value for ENVFILE is '.env'.
    requires python-dotenv.
    """
    check = dotenv_values(ENVFILE)[key]
    if check == "" or check == None:
        raise KeyError("The variable for {key} in {env} is empty".format(key=key, env=ENVFILE))
    return check
