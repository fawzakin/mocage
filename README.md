![MOCAGE LOGO](https://gitlab.com/fawzakin/mocage/-/raw/main/static/assets/logo.svg)
# You Got MOCAGEd!
Do you want to search your favorite action movie but your main search engine gives you too much information? Do you want to directly see what people think of your movie without clicking for movie detail? MOCAGE, Movie Catalogue Aggregator, got you covered!

Gathered from [FilmTV](https://www.filmtv.it) (don't ask why we are using an italian site), this simple webapp that you can locally host yourself will deliver a *good* movie searching experience directly onto your doorstep. Go MOCAGE yourself and enjoy the quick speed our service can offer.

# Requirements
- Python
- [Blazegraph](https://github.com/blazegraph/database/releases/latest)
- [FilmTV Turtle File](https://drive.google.com/file/d/1XTcsYWuiYAwCcWgk8oXLYBv05FKmbjZQ/view?usp=drive_link)

# First Run
Follow this step in your terminal to set up this Django repo in your machine locally:

1. Git clone this repo and cd into it.
    - `git clone https://gitlab.com/fawzakin/mocage.git mocage`
    - `cd mocage`
2. Create a local virtual environment called `.pyenv` inside this repo:
    - `python -m venv .pyenv`
    - `.pyenv\Scripts\Activate` if you are on Windows running Powershell
    - `source .pyenv/bin/activate` if you are on MacOS/Linux
3. Install all required package from `requirements.txt`:
    - `pip install -r requirements.txt`
4. Create an empty file called `.env` (don't add anything else into the filename. Literally just `.env`, not `.env.txt` or other crap) and add two lines of the following inside the file:
    - `DJANGO_SECRET_KEY=`
5. Get the secret key for Django by generate it yourself:
    - `python3 manage.py shell`
    - `from django.core.management.utils import get_random_secret_key`
    - `get_random_secret_key()`
    - Copy the string, including quotes, into your `.env` file as the vaue for `DJANGO_SECRET_KEY`. It should have exactly 52 characters including the quotes. Example (don't use the key from this example): `DJANGO_SECRET_KEY='4pPL3d4sH1$4r3ALsh!P4ndn0On3c4Nc0nV1nC3m30Th3rw1s3'`
6. Now you can run MOCAGE by activating the virtual environment everytime you open a new terminal session.
7. Run the webapp using Django server:
    - `python3 manage.py runserver 127.0.0.1:8000`

# Blazegraph Integration
To ensure that MOCAGE can quickly read from the turtle database, follow this direction to set up blazegraph.

1. [Download and launch Blazegraph from your terminal](https://github.com/blazegraph/database/releases/latest). The URL for Blazegraph after you run it should be something like `http://localhost:9999/blazegraph`.
2. Add `BLAZEGRAPH_URL` in the new line inside `.env` with the value of the URL you get previously plus `/sparql`. Below is an example.
    - `BLAZEGRAPH_URL="http://localhost:9999/blazegraph/sparql"`
3. Update your Blazegraph database using the turtle file you've downloaded. [If you haven't downloaded it yet, click here](https://drive.google.com/file/d/1XTcsYWuiYAwCcWgk8oXLYBv05FKmbjZQ/view?usp=drive_link). Open the first URL, go to UPDATE tab, type the absolute path to your turtle file, and click update. It should take less than a minute.
    - If you are on Windows, make sure you replace all '\' with '/' and add '/' at the beginning of the path. For example, if you put your turtle file in your download folder, the path would be at `/C:/Users/YourName/Downloads/filmtv2 (1).ttl`
5. Now MOCAGE can search your movie using blazegraph.

# Guidelines for our Developers
1. Use English on every part of this repository, including comments, variables, etc.
2. If you use a bloated IDE (PyCharm, IntelliJ IDE, etc.). Put their respective folder inside `.gitignore` folder.
3. Always make your own branch for your feature and create a merge request every time you want to add your changes to the `main` branch. Please refrain yourself from pushing your changes directly to `main` branch.

# License and Credit
GNU GPL V3.

Original dataset made by stefanoleone992, [taken from Kaggle](https://www.kaggle.com/datasets/stefanoleone992/filmtv-movies-dataset). Based of FilmTV's database.

Made for Semantic Web class.
