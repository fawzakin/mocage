from django.shortcuts import render
from search.query import get_movie
from urllib.error import URLError
from SPARQLWrapper.SPARQLExceptions import QueryBadFormed

def home(request):
    return render(request, 'home.html', context = {})

def search(request):
    if request.method == "POST":
        search = request.POST["search"]
        sort_by = request.POST.get("sort_by", "title")
        query = { "search": search }
        try:
            query["movies"] = get_movie(search, "title", sort_by)
        except URLError:
            query["error"] = "Our database isn't available at the moment :(" 
        except QueryBadFormed:
            query["error"] = "SPARQL error found in our search query :(" 
        return render(request, 'search.html', query)
    return render(request, 'search.html', context = {})
