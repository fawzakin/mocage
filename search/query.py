from SPARQLWrapper import JSON, SPARQLWrapper
from lib.env import get_env
from pprint import pprint

SEARCH = SPARQLWrapper(get_env("BLAZEGRAPH_URL"))
QUERY = """
prefix :      <http://dbpedia.org/resource/>
prefix film:  <https://www.filmtv.it/film/>
prefix owl:   <http://www.w3.org/2002/07/owl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix vcard: <http://www.w3.org/2006/vcard/ns#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT 
	?url 
	?title 
	?year 
        ?duration
	?genre_name 
        (GROUP_CONCAT(DISTINCT ?country_name; SEPARATOR=", ") AS ?country_names)
	(GROUP_CONCAT(DISTINCT ?dir_name; SEPARATOR=", ") AS ?dir_names)
	(GROUP_CONCAT(DISTINCT ?act_name; SEPARATOR=", ") AS ?act_names)
	?vote_avg 
	?vote_critic
	?vote_public
	?vote_total
	?desc
	?note
	?humor
	?rhythm
	?effort
	?tension
	?erotism
WHERE {{
    ?url :title ?title ;
         :year ?year ;
         :genre ?genre ;
         :duration ?duration ; 
         :country ?country ;
         :directors ?dir ;
	 :actors ?act ;
         :avg_vote ?vote_avg ;
	 :critics_vote ?vote_critic ;
	 :public_vote ?vote_public ;
         :total_vote ?vote_total ;
	 :description ?desc ;
	 :notes ?note ;
         :humor ?humor ;
         :rhythm ?rhythm ;
         :effort ?effort ;
         :tension ?tension ;
         :erotism ?erotism .
  	?genre rdf:label ?genre_name .
	?country rdf:label ?country_name .
	?dir rdf:label ?dir_name .
	?act rdf:label ?act_name .
  	filter(regex(lcase(?{0}),"{1}")) .
}}
GROUP BY
	?url 
	?title 
	?year 
        ?duration 
	?genre_name
	?country_names
	?dir_names
	?act_names
	?vote_avg 
	?vote_critic
	?vote_public
	?vote_total
	?desc
	?note
	?humor
	?rhythm
	?effort
	?tension
	?erotism
ORDER BY ?title ?year
"""

def sort_movies(movie_list, sort_key='rating', reverse=True):
    sorted_movies = sorted(movie_list, key=lambda x: x[sort_key], reverse=reverse)
    return sorted_movies

def get_movie(query: str, search_by: str, sort_key='rating', reverse=True) -> list[dict[str, str]]:
    SEARCH.setQuery(QUERY.format(search_by.lower(), query.lower()))
    SEARCH.setReturnFormat(JSON)
    result = SEARCH.query().convert()
    head_var = result["head"]["vars"]
    movie_list = []
    for res in result["results"]["bindings"]:
        movie_info = {}
        for var in head_var:
            movie_info[var] = res[var]["value"]
        movie_list.append(movie_info)

    # Sort the movie list based on the specified key
    sorted_movie_list = sort_movies(movie_list, sort_key=sort_key, reverse=reverse)

    return sorted_movie_list
